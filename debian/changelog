wannier90 (3.1.0+ds-8) unstable; urgency=medium

  * Team upload.
  * Mark strings raw to become compatible with Python3.12
    Closes: #1061858
  * Run autopkgtest with all supported Python3 versions
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 23 Feb 2024 14:38:12 +0100

wannier90 (3.1.0+ds-7) unstable; urgency=medium

  * Watching GitHub tags.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * Update standards version to 4.6.0, no changes needed.
  * Updating citation.
  * Improving patch descriptions.

 -- Andrius Merkys <merkys@debian.org>  Tue, 22 Feb 2022 01:12:29 -0500

wannier90 (3.1.0+ds-6) unstable; urgency=medium

  * Skipping a test failing on a bunch of architectures (Closes: #994554).

 -- Andrius Merkys <merkys@debian.org>  Mon, 20 Sep 2021 04:09:22 -0400

wannier90 (3.1.0+ds-5) unstable; urgency=medium

  * Skipping a test case failing with lapack 3.10 (Closes: #994475).
  * Updating description of skip_flaky_tests.patch.

 -- Andrius Merkys <merkys@debian.org>  Fri, 17 Sep 2021 07:03:14 -0400

wannier90 (3.1.0+ds-4) unstable; urgency=medium

  * Making build reproducible by forcing source date for building w90pov.pdf.
  * Adding 'Rules-Requires-Root: no'.

 -- Andrius Merkys <merkys@debian.org>  Thu, 17 Sep 2020 03:04:45 -0400

wannier90 (3.1.0+ds-3) unstable; urgency=medium

  * Bumping debhelper-compat (no changes).

 -- Andrius Merkys <merkys@debian.org>  Tue, 21 Jul 2020 23:50:24 -0400

wannier90 (3.1.0+ds-2) unstable; urgency=medium

  [ Michael Banck ]
  * debian/control (libwannier90-dev): New package.
  * debian/patches/libwannier_makefile.patch: New patch, add Makefile support
    to install the static library into a configurable libdir and the Fortran90
    modules into /usr/include.
  * debian/rules (override_dh_auto_install): New target, overrides LIBDIR for
    the install target with the proper multiarch libdir.

  [ Andrius Merkys ]
  * Switching to debhelper-compat dependency.

 -- Andrius Merkys <merkys@debian.org>  Tue, 07 Jul 2020 03:43:38 -0400

wannier90 (3.1.0+ds-1) unstable; urgency=medium

  * New upstream version 3.1.0+ds
  * Refreshing patches.
  * Simplifying debian/wannier90.install.
  * Moving manpages to debian/man/.
  * Adding a manpage for w90spn2spn.x.1.
  * Improving the cleaning action.

 -- Andrius Merkys <merkys@debian.org>  Thu, 26 Mar 2020 03:27:13 -0400

wannier90 (3.0.0+ds-4) unstable; urgency=medium

  * Forcing python3 for test drivers.

 -- Andrius Merkys <merkys@debian.org>  Tue, 31 Dec 2019 06:17:00 -0500

wannier90 (3.0.0+ds-3) unstable; urgency=medium

  * Adding a patch to skip flaky tests.

 -- Andrius Merkys <merkys@debian.org>  Mon, 30 Dec 2019 03:36:27 -0500

wannier90 (3.0.0+ds-2) unstable; urgency=medium

  * Improving debian/copyright.
  * Using full names in debian/copyright.
  * Build-depending on python3 instead of python.
  * Autopkgtest runner depends on python3.

 -- Andrius Merkys <merkys@debian.org>  Fri, 27 Dec 2019 04:01:27 -0500

wannier90 (3.0.0+ds-1) unstable; urgency=medium

  * Initial release (Closes: #578829)

 -- Andrius Merkys <merkys@debian.org>  Mon, 01 Apr 2019 01:55:26 -0400
